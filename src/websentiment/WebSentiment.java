/*
 * MUHAMMAD Ahsan
 * <muhammad.ahsan@gmail.com>
 */
package websentiment;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

/**
 *
 * @author ahsan
 */
public class WebSentiment {

    public static String Path1 = "positive-words.csv";
    public static String Path2 = "negative-words.csv";
    // Path of stopwords used in English Language
    public static String Path5 = "mit-Stopwords.txt";
    // Vector to hold stop words
    public static Collection<String> Stopword = new LinkedList<>();
    // Vector to hold positive words
    public static Collection<String> PositiveWord = new LinkedList<>();
    // Vector to hold negative vectors
    public static Collection<String> NegativeWord = new LinkedList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        String URL = "http://www.facebook.com";
        String P = WebSentimentScore(URL);
        if (P.equals("P")) {
            System.out.println("Positive sentiment from " + URL);
        } else {
            System.out.println("Negative sentiment from " + URL);

        }
    }

    public static String WebSentimentScore(String URI) throws FileNotFoundException, IOException, ClassNotFoundException {
        if (Stopword.isEmpty()) {
            Initialize();
        }
        String content = ScrapContent(URI);
        String Result = ClassifierStringDoc(content);
        return Result;
    }

    public static String ClassifierStringDoc(String aFileName) throws IOException, ClassNotFoundException {
        if (Stopword.isEmpty()) {
            Initialize();
        }

        // Variable to compute
        int PCount = 0;
        int NCount = 0;
        try (Scanner scanner = new Scanner(aFileName)) {
            // Process each line which constitute the phase of content extraction
            while (scanner.hasNextLine()) {
                String test = scanner.nextLine();
                Scanner tokenize = new Scanner(test);
                while (tokenize.hasNext()) {
                    // Process of selecting UNIGRAM in sentence
                    String word = tokenize.next().toUpperCase();

                    /*
                     * Process of Stopword removal
                     * This option reduces the performace of positive cases but
                     * improves the negative cases to alomost perfection.
                     *
                     *
                     * Syntactic tagging for adjective filtering
                     */

                    // Validation of word in English Dictionary
                    boolean isPOS = PositiveWord.contains(word);
                    boolean isNEG = NegativeWord.contains(word);
                    // Successful Hit count increments
                    if (isPOS == true) {
                        PCount++;
                    }
                    // Unsuccessful Hit count increments
                    if (isNEG == true) {
                        NCount++;
                    }
                }
            }
            if (PCount > NCount) {
                return "P";
            } else {
                return "N";
            }
        } catch (Exception e) {
            throw new IllegalAccessError("Cannot open POS Tagger");
        }
    }

    public static String ScrapContent(String URI) throws IOException {
        if (!URI.isEmpty()) {
            try {
                // Loading the page in Document
                org.jsoup.nodes.Document D = Jsoup.connect(URI).get();
                // Reading only paragraphs
                Elements ps = D.select("p");
                return ps.text();
            } catch (Exception e) {
                throw new IllegalArgumentException(e.toString());
            }
        } else {
            throw new IllegalArgumentException("URI not valid");
        }

    }

    // House keeping function for loading dictionaries and data to be used and processed by program
    private static void Initialize() throws FileNotFoundException, IOException {
        // CSV file reading through OpenCSV
        CSVReader RP = new CSVReader(new FileReader(Path1));
        List<String[]> PositiveEntries = RP.readAll();
        // Positive sentiment Dictionary Loading
        for (int i = 0; i < PositiveEntries.size(); i++) {
            PositiveWord.add(PositiveEntries.get(i)[0].toString().toUpperCase());
        }
        CSVReader RN = new CSVReader(new FileReader(Path2));
        List<String[]> NegativeEntries = RN.readAll();
        // Negative sentiment Dictionary Loading
        for (int i = 0; i < NegativeEntries.size(); i++) {
            String str = NegativeEntries.get(i)[0].toString().toUpperCase();
            NegativeWord.add(str);
        }
        // MIT stop words dictionary Loading
        CSVReader SW = new CSVReader(new FileReader(Path5));
        List<String[]> SWord = SW.readAll();
        // Negative sentiment Dictionary Loading
        for (int i = 0; i < SWord.size(); i++) {
            String str = SWord.get(i)[0].toString().toUpperCase();
            Stopword.add(str);
        }
    }
}
